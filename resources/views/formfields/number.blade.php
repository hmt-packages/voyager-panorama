<div class="pull-right">
    <span class="label label-default">
        <span id="number-{{ $row->field }}">{{ $dataTypeContent->{$row->field} ?? $options->default ?? '' }}</span>
        <a type="button" class="btn-select-number"><i class="voyager-down-circled"></i></a>
    </span>
</div>

<input type="number"
       class="form-control"
       name="{{ $row->field }}"
       @if($row->required) required @endif
       @if(isset($options->min)) min="{{ $options->min }}" @endif
       @if(isset($options->max)) max="{{ $options->max }}" @endif
       step="{{ $options->step ?? 'any' }}"
       placeholder="{{ old($row->field, $options->placeholder ?? $row->getTranslatedAttribute('display_name')) }}"
       value="{{ old($row->field, $dataTypeContent->{$row->field} ?? $options->default ?? '') }}">
