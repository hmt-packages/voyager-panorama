<select class="form-control hotspot-scene" name="{{ $row->field }}" id="{{ $row->field }}">
    <option></option>
    @foreach ($scenes as $scene)
        <option value="scene_{{ $scene->id }}" {{ "scene_{$scene->id}" != $hotspot->sceneId ?: 'selected' }}>
            {{ $scene->title }}
        </option>
    @endforeach
</select>
