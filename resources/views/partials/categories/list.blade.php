<ol class="dd-list">
    @foreach ($results as $result)
        @includeIf('panorama::partials.categories.item')
    @endforeach
</ol>
