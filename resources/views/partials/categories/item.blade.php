<li class="dd-item" data-id="{{ $result->id }}">
    <div class="dd-handle">{{ $result->name }}</div>
    @includeWhen(!blank($result->children), 'panorama::partials.categories.list', [
        'results' => $result->children
    ])
</li>
