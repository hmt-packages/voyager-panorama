<!DOCTYPE HTML>

<html>
<head>
    <meta charset=utf-8>
    <meta name=viewport content="width=device-width, initial-scale=1.0">
    <title>Panorama</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pannellum/2.5.6/pannellum.css" integrity="sha512-UoT/Ca6+2kRekuB1IDZgwtDt0ZUfsweWmyNhMqhG4hpnf7sFnhrLrO0zHJr2vFp7eZEvJ3FN58dhVx+YMJMt2A==" crossorigin="anonymous" />
    <style type="text/css">
        html {
            height: 100%
        }

        body {
            margin: 0;
            padding: 0;
            overflow: hidden;
            position: fixed;
            cursor: default;
            width: 100%;
            height: 100%
        }
    </style>
</head>
<body>
<div id=container>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pannellum/2.5.6/pannellum.js" integrity="sha512-EmZuy6vd0ns9wP+3l1hETKq/vNGELFRuLfazPnKKBbDpgZL0sZ7qyao5KgVbGJKOWlAFPNn6G9naB/8WnKN43Q==" crossorigin="anonymous"></script>
<script type="text/javascript">
    let url = new URL(location.href);
    let config_url = url.searchParams.get('config');

    $.getJSON(config_url, function(data) {
        pannellum.viewer('container', data);
    });
</script>
</body>
</html>
