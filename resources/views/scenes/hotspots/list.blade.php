<div class="col-md-3">
    <ul class="nav nav-pills nav-stacked" role="tablist">
        @foreach ($hotspots as $hotspot)
            @include('panorama::scenes.hotspots.item')
        @endforeach
    </ul>
    <button type="button" class="btn btn-primary" id="btn-add-hotspot" data-url="{{ route("voyager.{$dataType->slug}.hotspots.create") }}">
        <i class="voyager-list-add"></i> Add hotspot
    </button>
    <div class="form-group">
        <input type="checkbox" name="target-tool" id="target-tool" class="form-control toggleswitch">
        <label for="target-tool">Target tool</label>
        <p class="help-block">
            Use the target tool to quick a hotspot and is's a location on the panorama.
            <br>
            When the target tool is <i class="fa fa-toggle-on"></i> click on the panorama together with Ctrl key.
        </p>
    </div>
</div>

<div class="tab-content col-md-9 jumbotron">
    @php
        $scenes = $repository->get($dataTypeContent->id ?? null, true);
        $edit = true;
    @endphp
    @foreach ($hotspots as $hotspot)
        @include('panorama::scenes.hotspots.content')
    @endforeach
</div>

<input type="hidden" name="hotspots[destroy]" value="[]">
