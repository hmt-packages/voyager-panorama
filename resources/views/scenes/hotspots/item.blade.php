@php($hotspotId = $hotspot->id ?: 'new_' . request()->get('key', 1))

<li role="presentation" class="{{ isset($loop) && $loop->first ? 'active' : ''}}" data-hotspot_id="{{ $hotspotId }}">
    <a href="#hotspot_{{ $hotspotId }}" aria-controls="hotspot_{{ $hotspotId }}" role="tab" data-toggle="tab">
        {{ $hotspot->text ?: 'New' }}
    </a>
    <a type="button" class="close btn-remove-hotspot" aria-label="Close" data-remove_id="{{ $hotspotId }}"><span aria-hidden="true">&times;</span></a>
</li>
