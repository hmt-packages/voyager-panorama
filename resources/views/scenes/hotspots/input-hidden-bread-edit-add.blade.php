@if (in_array($field, $hotspot->getTranslatableAttributes()))
    @php($row->i18n_field = "hotspots[{$hotspotId}][i18n][{$field}]")
    <span class="language-label js-language-label">{{ config('voyager.multilingual.default') }}</span>
    <input type="hidden"
           data-i18n="true"
           name="{{ $row->i18n_field }}"
           id="{{ $row->i18n_field }}"
           value="{{ old($row->i18n_field, get_field_translations($hotspot, $field)) }}">
@endif
