@php
    $dataType = \TCG\Voyager\Facades\Voyager::model('DataType')->where('name', 'panorama_hotspots')->first();
    $dataRows = $dataType->rows()->where('edit', true)->get();
    $hotspotId = $hotspot->id ?: 'new_' . request()->get('key', 1);
@endphp

<div role="tabpanel" class="tab-pane fade {{ isset($loop) && $loop->first ? 'in active' : ''}}" id="hotspot_{{ $hotspotId }}" data-hotspot_id="{{ $hotspotId }}">
    <div class="form-group col-md-12">
        @foreach($dataRows as $row)
            @php
                $field = $row->field;
                $row->field = "hotspots[{$hotspotId}][{$field}]";
                $dataTypeContent->{$row->field} = $hotspot->{$field};
            @endphp
            @include('panorama::partials.hotspots.field')
        @endforeach
    </div>
</div>
