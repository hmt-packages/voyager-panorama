$(document).ready(function () {
    const sceneId = `scene_${$('.page-content').data('id') || 'new'}`;

    let viewer = initViewer(sceneId);

    $('input[name="panorama"]').change(function () {
        viewer = rerenderViewer(viewer, sceneId);
    });

    $('#btn-preview').click(function () {
        loadReview(viewer, sceneId);
    });

    $('#btn-add-hotspot').click(function () {
        addHotSpot();
    });

    $('#hotspots').on('click', '.btn-remove-hotspot', function () {
        let hotspotId = $(this).data('remove_id');
        removeHotSpot(viewer, hotspotId, sceneId);
    });

    $('.btn-select-number').click(function () {
        $(this).closest('.form-group').find('input').val(parseFloat($(this).parent().text()));
    });

    $('[name*="[type]"]').each(function () {
        execType($(this));
    });

    $('#hotspots').on('change', '[name*="[type]"]', function () {
        execType($(this));
    });

    $('#hotspots').on('change', 'input', function () {
        let i18n = $(this).parent().find('input[data-i18n]');

        if (i18n.length) {
            let lang = $('.active input[name="i18n_selector"]').attr('id');
            let i18nData = JSON.parse(i18n.val());

            i18nData[lang] = $(this).val();
            i18n.val(JSON.stringify(i18nData));

            let hotspotId = $(this).closest('.tab-pane').data('hotspot_id');
            $(`#hotspots ul li[data-hotspot_id="${hotspotId}"] a:first`).text($(this).val());
        }
    });

    $('input[name="i18n_selector"]').change(function () {
        let lang = $(this).attr('id');

        $('#hotspots .tab-content .tab-pane input[data-i18n="true"]').each(function () {
            let hotspotId = $(this).closest('.tab-pane').data('hotspot_id');
            let i18nData = JSON.parse($(this).val());

            $(`#hotspots ul li[data-hotspot_id="${hotspotId}"] a:first`).text(i18nData[lang] || 'New');
            $(this).parent().children('input:last').val(i18nData[lang]);
        });
    });
});

function initViewer(sceneId) {
    let sceneOption = {};
    let scenesOption = $('.page-content').data('scenes');

    if (sceneId !== 'scene_new') {
        let panorama = $('div[data-field-name="panorama"] img').attr('src');
        sceneOption = getSceneOption(panorama);
    }

    let viewer = pannellum.viewer('panorama', {
        default: {
            firstScene: sceneId,
        },
        scenes: {...{[sceneId]: sceneOption}, ...scenesOption}
    });

    return execEvents(viewer, sceneId);
}

function rerenderViewer(viewer, sceneId) {
    let files = $('input[name="panorama"]').prop('files');
    let panorama = window.URL.createObjectURL(files[0]);

    let sceneOption = getSceneOption(panorama);
    let scenesOption = ($('.page-content').data('scenes'));

    viewer.destroy();

    viewer = pannellum.viewer('panorama', {
        default: {
            firstScene: sceneId,
        },
        scenes: Object.assign({[sceneId]: sceneOption}, scenesOption)
    });

    return execEvents(viewer, sceneId);
}

function getSceneOption(panorama) {
    return {
        title: $('input[name="title"]').val(),
        hfov: parseFloat($('input[name="hfov"]').val()) || 100,
        pitch: parseFloat($('input[name="pitch"]').val()) || 0,
        yaw: parseFloat($('input[name="yaw"]').val()) || 0,
        type: 'equirectangular',
        panorama: panorama,
        autoLoad: $('input[name="autoLoad"]').is(':checked') || true,
        autoRotate: parseFloat($('input[name="autoRotate"]').val()) || 0,
        showZoomCtrl: $('input[name="showZoomCtrl"]').is(':checked') || true,
        showFullscreenCtrl: $('input[name="showFullscreenCtrl"]').is(':checked') || true,
        keyboardZoom: $('input[name="keyboardZoom"]').is(':checked') || true,
        disableKeyboardCtrl: $('input[name="disableKeyboardCtrl"]').is(':checked') || false,
        mouseZoom: $('input[name="mouseZoom"]').is(':checked') || true,
        draggable: $('input[name="draggable"]').is(':checked') || true,
        compass: $('input[name="compass"]').is(':checked') || true,
    }
}

function execEvents(viewer, sceneId) {
    return viewer.on('load', function () {
        $('#hotspots .tab-content .tab-pane').each(function () {
            let hotspotId = $(this).attr('id');

            viewer.removeHotSpot(hotspotId, sceneId);
            viewer.addHotSpot({
                pitch: parseFloat($(this).find('input[name*="[pitch]"]').val()),
                yaw: parseFloat($(this).find('input[name*="[yaw]"]').val()),
                type: $(this).find('select[name*="[type]"]').val(),
                text: $(this).find('input[name*="[text]"]:last').val(),
                URL: $(this).find('input[name*="[URL]"]').val(),
                sceneId: $(this).find('select[name*="[sceneId]"]').val(),
                id: $(this).attr('id'),
            }, sceneId);

            if ($('input[name="autoRotate"]').val() !== 0) {
                viewer.startAutoRotate($('input[name="autoRotate"]').val());
            } else {
                viewer.stopAutoRotate();
            }
        });
    }).on('animatefinished', function () {
        $('#number-yaw').text((viewer.getYaw() || 0).toFixed(2));
        $('#number-pitch').text((viewer.getPitch() || 0).toFixed(2));
        $('#number-hfov').text((viewer.getHfov() || 100).toFixed(2));
    }).on('mousedown', function (e) {
        if ($('#target-tool').is(':checked') && e.ctrlKey && viewer.getScene() === sceneId) {
            let coords = viewer.mouseEventToCoords(e);
            let hotspotId = ($('#hotspots').data('new') || 0) + 1;
            let url = $('#btn-add-hotspot').data('url');

            $.get(url, {
                key: hotspotId,
                scene_id: parseInt($('.page-content').data('id')) || null,
                pitch: coords[0].toFixed(2),
                yaw: coords[1].toFixed(2),
            }, function (data) {
                $('#hotspots div ul').append(data.item);
                $('#hotspots .tab-content').append(data.content);

                $('#hotspots').data('new', hotspotId);
                $('#hotspots div ul li:last-child a').tab('show');
                execType($('#hotspots [name*="[type]"]:last'));

                viewer.addHotSpot({
                    pitch: coords[0].toFixed(2),
                    yaw: coords[1].toFixed(2),
                    type: 'info',
                    text: 'New',
                    id: `hotspot_new_${hotspotId}`,
                }, sceneId);
            }).then(function () {
            });
        }
    });
}

function loadReview(viewer, sceneId) {
    let scenePitch = parseFloat($('input[name="pitch"]').val());
    let sceneYaw = parseFloat($('input[name="yaw"]').val());
    let sceneHfov = parseFloat($('input[name="hfov"]').val());

    viewer.loadScene(sceneId, scenePitch, sceneYaw, sceneHfov);
}

function addHotSpot() {
    let url = $('#btn-add-hotspot').data('url');
    let hotspotId = ($('#hotspots').data('new') || 0) + 1;

    $.get(url, {key: hotspotId, scene_id: parseInt($('.page-content').data('id')) || null}, function (data) {
        $('#hotspots div ul').append(data.item);
        $('#hotspots .tab-content').append(data.content);
    }).then(function () {
        $('#hotspots').data('new', hotspotId);
        execType($('#hotspots [name*="[type]"]:last'));
    });
}

function removeHotSpot(viewer, hotspotId, sceneId) {
    if (parseInt(hotspotId)) {
        let value = JSON.parse($('#hotspots [name="hotspots[destroy]"]').val());
        value.push(hotspotId);
        $('#hotspots [name="hotspots[destroy]"]').val(JSON.stringify(value));
    }
    $(`#hotspots ul li[data-hotspot_id=${hotspotId}]`).remove();
    $(`#hotspots .tab-content .tab-pane[data-hotspot_id=${hotspotId}]`).remove();
    viewer.removeHotSpot(`hotspot_${hotspotId}`, sceneId);
}

function execType(element) {
    if (element.val() === 'info') {
        element.parent().parent().find('[name*="[URL]"]').attr('disabled', false);
        element.parent().parent().find('[name*="[sceneId]"]').attr('disabled', true).attr('required', false);
    } else if (element.val() === 'scene') {
        element.parent().parent().find('[name*="[URL]"]').attr('disabled', true);
        element.parent().parent().find('[name*="[sceneId]"]').attr('disabled', false).attr('required', true);
    }
}
