<?php

return [
    'labels'  => [
        'media'         => 'Media',
        'options'       => 'Options',
        'relationships' => 'Relationships',
    ],
    'hotspot' => [
        'pitch' => 'Pitch',
        'scene' => 'Scene',
        'text'  => 'Text',
        'type'  => 'Type',
        'url'   => 'URL',
        'yaw'   => 'Yaw',

        'types' => [
            'info'  => 'Info',
            'scene' => 'Scene',
        ],
    ],
];
