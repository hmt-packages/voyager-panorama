<?php

return [
    'data_rows' => [
        'autoLoad'               => 'Auto load',
        'autoRotate'             => 'Auto rotate',
        'compass'                => 'Compass',
        'description'            => 'Description',
        'disableKeyboardCtrl'    => 'Disable keyboard controls',
        'draggable'              => 'Draggable',
        'hfov'                   => 'Hfov',
        'keyboardZoom'           => 'Keyboard zoom',
        'mouseZoom'              => 'Mouse zoom',
        'orientationOnByDefault' => 'Orientation on by default',
        'panorama'               => 'Panorama',
        'parent-category'        => 'Parent category',
        'pitch'                  => 'Pitch',
        'scene'                  => 'Scene',
        'scenes'                 => 'Scenes',
        'showControls'           => 'Show controls',
        'showFullscreenCtrl'     => 'Show fullscreen control',
        'showZoomCtrl'           => 'Show zoom control',
        'sub-categories'         => 'Sub-categories',
        'text'                   => 'Text',
        'type'                   => 'Type',
        'url'                    => 'URL',
        'yaw'                    => 'Yaw',
    ],
];
