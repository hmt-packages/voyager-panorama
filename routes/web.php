<?php

use HMT\Panorama\Models\Scene;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use TCG\Voyager\Http\Middleware\VoyagerAdminMiddleware;

Route::middleware(['web', VoyagerAdminMiddleware::class])
    ->namespace('HMT\Panorama\Http\Controllers\Admin')
    ->prefix('admin')
    ->name('voyager.')
    ->group(function () {
        Route::prefix($sceneTableSlug = Str::slug(app(Scene::class)->getTable()))
            ->name("{$sceneTableSlug}.")
            ->group(function () {
                Route::get('/config', 'SceneController@config')->name('config');
                Route::get('/review', 'SceneController@review')->name('review');

                Route::prefix('hotspots')
                    ->name('hotspots.')
                    ->group(function () {
                        Route::get('/create', 'HotspotController@create')->name('create');
                    });
            });
    });
