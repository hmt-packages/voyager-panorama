<?php

namespace HMT\Panorama\Database\Seeders;

use HMT\Panorama\Http\Controllers\Admin\SceneController;
use HMT\Panorama\Models\Category;
use HMT\Panorama\Models\Scene;

class SceneSeeder extends AbstractSeeder
{
    public function __construct(Scene $model, SceneController $controller)
    {
        parent::__construct($model, $controller, $model->getTable(), 'voyager-photos');
    }

    protected function buildData()
    {
        // TODO: Implement buildData() method.
    }

    protected function buildCRUD()
    {
        //Data Type
        $dataType = $this->_buildDataType([
            'order_column'         => 'order',
            'order_display_column' => 'title',
            'order_direction'      => 'asc',
            'default_search_key'   => null,
            'scope'                => null,
        ]);

        //Data Rows
        $dataRows = [
            [
                'attributes'    => [
                    'field'         => 'id',
                ],
                'values'        => [
                    'type'          => 'number',
                    'display_name'  => __('voyager::seeders.data_rows.id'),
                    'required'      => 1,
                    'browse'        => 0,
                    'read'          => 0,
                    'edit'          => 0,
                    'add'           => 0,
                    'delete'        => 0,
                    'order'         => 1,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'active'
                ],
                'values'        => [
                    'type'          => 'checkbox',
                    'display_name'  => __('voyager::seeders.data_rows.status'),
                    'required'      => 1,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'order'         => 2,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'title'
                ],
                'values'        => [
                    'type'          => 'text',
                    'display_name'  => __('voyager::seeders.data_rows.title'),
                    'required'      => 1,
                    'browse'        => 1,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'order'         => 3,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'panorama'
                ],
                'values'        => [
                    'type'          => 'image',
                    'display_name'  => __('panorama::seeders.data_rows.panorama'),
                    'required'      => 1,
                    'browse'        => 1,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 0,
                    'order'         => 4,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'yaw'
                ],
                'values'        => [
                    'type'          => 'number',
                    'display_name'  => __('panorama::seeders.data_rows.yaw'),
                    'required'      => 1,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'view'          => 'panorama::formfields.number',
                        'display'       => [
                            'width'         => 4
                        ],
                        'default'       => 0,
                        'min'           => -180,
                        'max'           => 180,
                    ],
                    'order'         => 5,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'pitch'
                ],
                'values'        => [
                    'type'          => 'number',
                    'display_name'  => __('panorama::seeders.data_rows.pitch'),
                    'required'      => 1,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'view'          => 'panorama::formfields.number',
                        'display'       => [
                            'width'         => 4
                        ],
                        'default'       => 0,
                        'min'           => -90,
                        'max'           => 90,
                    ],
                    'order'         => 6,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'hfov'
                ],
                'values'        => [
                    'type'          => 'number',
                    'display_name'  => __('panorama::seeders.data_rows.hfov'),
                    'required'      => 1,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'view'          => 'panorama::formfields.number',
                        'display'       => [
                            'width'         => 4
                        ],
                        'default'       => 100,
                        'min'           => 50,
                        'max'           => 120,
                    ],
                    'order'         => 7,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'autoRotate'
                ],
                'values'        => [
                    'type'          => 'number',
                    'display_name'  => __('panorama::seeders.data_rows.autoRotate'),
                    'required'      => 0,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'display'       => [
                            'width'         => 12
                        ],
                        'default'       => 0
                    ],
                    'order'         => 8,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'autoLoad'
                ],
                'values'        => [
                    'type'          => 'checkbox',
                    'display_name'  => __('panorama::seeders.data_rows.autoLoad'),
                    'required'      => 1,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'display'       => [
                            'width'         => 6
                        ],
                        'checked'       => true,
                    ],
                    'order'         => 9,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'showZoomCtrl'
                ],
                'values'        => [
                    'type'          => 'checkbox',
                    'display_name'  => __('panorama::seeders.data_rows.showZoomCtrl'),
                    'required'      => 1,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'display'       => [
                            'width'         => 6
                        ],
                        'checked'       => true,
                    ],
                    'order'         => 10,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'showFullscreenCtrl'
                ],
                'values'        => [
                    'type'          => 'checkbox',
                    'display_name'  => __('panorama::seeders.data_rows.showFullscreenCtrl'),
                    'required'      => 1,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'display'       => [
                            'width'         => 6
                        ],
                        'checked'       => true,
                    ],
                    'order'         => 11,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'keyboardZoom'
                ],
                'values'        => [
                    'type'          => 'checkbox',
                    'display_name'  => __('panorama::seeders.data_rows.keyboardZoom'),
                    'required'      => 1,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'display'       => [
                            'width'         => 6
                        ],
                        'checked'       => true,
                    ],
                    'order'         => 12,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'disableKeyboardCtrl'
                ],
                'values'        => [
                    'type'          => 'checkbox',
                    'display_name'  => __('panorama::seeders.data_rows.disableKeyboardCtrl'),
                    'required'      => 1,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'display'       => [
                            'width'         => 6
                        ],
                        'checked'       => false,
                    ],
                    'order'         => 13,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'mouseZoom'
                ],
                'values'        => [
                    'type'          => 'checkbox',
                    'display_name'  => __('panorama::seeders.data_rows.mouseZoom'),
                    'required'      => 1,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'display'       => [
                            'width'         => 6
                        ],
                        'checked'       => true,
                    ],
                    'order'         => 14,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'draggable'
                ],
                'values'        => [
                    'type'          => 'checkbox',
                    'display_name'  => __('panorama::seeders.data_rows.draggable'),
                    'required'      => 1,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'display'       => [
                            'width'         => 6
                        ],
                        'checked'       => true,
                    ],
                    'order'         => 15,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'compass'
                ],
                'values'        => [
                    'type'          => 'checkbox',
                    'display_name'  => __('panorama::seeders.data_rows.compass'),
                    'required'      => 1,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'display'       => [
                            'width'         => 6
                        ],
                        'checked'       => true,
                    ],
                    'order'         => 16,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'category_id'
                ],
                'values'        => [
                    'type'          => 'select_dropdown',
                    'display_name'  => __('voyager::seeders.data_rows.category'),
                    'required'      => 0,
                    'browse'        => 1,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'default'       => '',
                        'null'          => '',
                        'options'       => [
                            ''              => '-- None --',
                        ],
                        'relationship'  => [
                            'key'           => 'id',
                            'label'         => 'name',
                        ],
                    ],
                    'order'         => 17,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'panorama_scene_belongsto_panorama_category_relationship'
                ],
                'values'        => [
                    'type'          => 'relationship',
                    'display_name'  => __('voyager::seeders.data_rows.category'),
                    'required'      => 1,
                    'browse'        => 1,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'model'         => Category::class,
                        'table'         => app(Category::class)->getTable(),
                        'type'          => 'belongsTo',
                        'column'        => 'category_id',
                        'key'           => 'id',
                        'label'         => 'name',
                        'pivot_table'   => '',
                        'pivot'         => '0',
                        'taggable'      => null,
                    ],
                    'order'         => 17,
                ],
            ],
            [
                'attributes'    => [
                    'field'         => 'created_at',
                ],
                'values'        => [
                    'type'          => 'timestamp',
                    'display_name'  => __('voyager::seeders.data_rows.created_at'),
                    'required'      => 0,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 0,
                    'add'           => 0,
                    'delete'        => 0,
                    'order'         => 18,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'updated_at',
                ],
                'values'        => [
                    'type'          => 'timestamp',
                    'display_name'  => __('voyager::seeders.data_rows.updated_at'),
                    'required'      => 0,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 0,
                    'add'           => 0,
                    'delete'        => 0,
                    'order'         => 19,
                ]
            ],
        ];
        $this->_buildDataRows($dataType, $dataRows);
    }

    protected function buildMenu()
    {
        $this->_buildMenu('Scenes', 2, [
            'title'      => 'Panorama',
            'url'        => '',
            'icon_class' => 'voyager-photos',
            'order'      => 4,
        ]);
    }

    protected function buildPermission()
    {
        $this->_buildPermission(['browse', 'read', 'edit', 'add', 'delete', 'active']);
    }
}
