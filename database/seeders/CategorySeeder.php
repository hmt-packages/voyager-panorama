<?php

namespace HMT\Panorama\Database\Seeders;

use HMT\Panorama\Http\Controllers\Admin\CategoryController;
use HMT\Panorama\Models\Scene;
use HMT\Panorama\Models\Category;

class CategorySeeder extends AbstractSeeder
{
    public function __construct(Category $model, CategoryController $controller)
    {
        parent::__construct($model, $controller, $model->getTable(), 'voyager-categories');
    }

    protected function buildData()
    {
        //
    }

    protected function buildCRUD()
    {
        //Data Type
        $dataType = $this->_buildDataType([
            'order_column'         => 'order',
            'order_display_column' => 'name',
            'order_direction'      => 'asc',
            'default_search_key'   => null,
            'scope'                => null,
        ]);

        //Data Rows
        $dataRows = [
            [
                'attributes'    => [
                    'field'         => 'id',
                ],
                'values'        => [
                    'type'          => 'number',
                    'display_name'  => __('voyager::seeders.data_rows.id'),
                    'required'      => 1,
                    'browse'        => 0,
                    'read'          => 0,
                    'edit'          => 0,
                    'add'           => 0,
                    'delete'        => 0,
                    'order'         => 1,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'active'
                ],
                'values'        => [
                    'type'          => 'checkbox',
                    'display_name'  => __('voyager::seeders.data_rows.status'),
                    'required'      => 1,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'order'         => 2,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'name'
                ],
                'values'        => [
                    'type'          => 'text',
                    'display_name'  => __('voyager::seeders.data_rows.name'),
                    'required'      => 1,
                    'browse'        => 1,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'order'         => 3,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'parent_id'
                ],
                'values'        => [
                    'type'          => 'select_dropdown',
                    'display_name'  => __('panorama::seeders.data_rows.parent-category'),
                    'required'      => 0,
                    'browse'        => 1,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'default'       => '',
                        'null'          => '',
                        'options'       => [
                            ''              => '-- None --',
                        ],
                        'relationship'  => [
                            'key'           => 'id',
                            'label'         => 'name',
                        ],
                    ],
                    'order'         => 4,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'panorama_category_belongsto_panorama_category_relationship'
                ],
                'values'        => [
                    'type'          => 'relationship',
                    'display_name'  => __('panorama::seeders.data_rows.parent-category'),
                    'required'      => 0,
                    'browse'        => 1,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'model'         => get_class($this->model),
                        'table'         => $this->model->getTable(),
                        'type'          => 'belongsTo',
                        'column'        => 'parent_id',
                        'key'           => 'id',
                        'label'         => 'name',
                        'pivot_table'   => '',
                        'pivot'         => '0',
                        'taggable'      => null,
                    ],
                    'order'         => 4,
                ],
            ],
            [
                'attributes'    => [
                    'field'         => 'panorama_category_hasmany_panorama_category_relationship'
                ],
                'values'        => [
                    'type'          => 'relationship',
                    'display_name'  => __('panorama::seeders.data_rows.sub-categories'),
                    'required'      => 0,
                    'browse'        => 1,
                    'read'          => 1,
                    'edit'          => 0,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'model'         => get_class($this->model),
                        'table'         => $this->model->getTable(),
                        'type'          => 'hasMany',
                        'column'        => 'parent_id',
                        'key'           => 'id',
                        'label'         => 'name',
                        'pivot_table'   => '',
                        'pivot'         => '0',
                        'taggable'      => null,
                    ],
                    'order'         => 5,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'panorama_category_hasmany_panorama_scene_relationship'
                ],
                'values'        => [
                    'type'          => 'relationship',
                    'display_name'  => __('panorama::seeders.data_rows.scenes'),
                    'required'      => 0,
                    'browse'        => 1,
                    'read'          => 1,
                    'edit'          => 0,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'model'         => Scene::class,
                        'table'         => app(Scene::class)->getTable(),
                        'type'          => 'hasMany',
                        'column'        => 'category_id',
                        'key'           => 'id',
                        'label'         => 'title',
                        'pivot_table'   => '',
                        'pivot'         => '0',
                        'taggable'      => null,
                    ],
                    'order'         => 6,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'created_at',
                ],
                'values'        => [
                    'type'          => 'timestamp',
                    'display_name'  => __('voyager::seeders.data_rows.created_at'),
                    'required'      => 0,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 0,
                    'add'           => 0,
                    'delete'        => 0,
                    'order'         => 7,
                ],
            ],
            [
                'attributes'    => [
                    'field'         => 'updated_at',
                ],
                'values'        => [
                    'type'          => 'timestamp',
                    'display_name'  => __('voyager::seeders.data_rows.updated_at'),
                    'required'      => 0,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 0,
                    'add'           => 0,
                    'delete'        => 0,
                    'order'         => 8,
                ]
            ],
        ];
        $this->_buildDataRows($dataType, $dataRows);
    }

    protected function buildMenu()
    {
        $this->_buildMenu('Categories', 1, [
            'title'      => 'Panorama',
            'url'        => '',
            'icon_class' => 'voyager-photos',
            'order'      => 4,
        ]);
    }

    protected function buildPermission()
    {
        $this->_buildPermission(['browse', 'read', 'edit', 'add', 'delete', 'active']);
    }
}
