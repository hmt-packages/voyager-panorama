<?php

namespace HMT\Panorama\Database\Seeders;

use HMT\Panorama\Http\Controllers\Admin\HotspotController;
use HMT\Panorama\Models\Hotspot;
use HMT\Panorama\Repositories\HotspotRepository;

class HotspotSeeder extends AbstractSeeder
{
    public function __construct(Hotspot $model, HotspotController $controller)
    {
        parent::__construct($model, $controller);
    }

    protected function buildData()
    {
        //
    }

    protected function buildCRUD()
    {
        //Data Type
        $dataType = $this->_buildDataType();

        //Data Rows
        $dataRows = [
            [
                'attributes'    => [
                    'field'         => 'id',
                ],
                'values'        => [
                    'type'          => 'number',
                    'display_name'  => __('voyager::seeders.data_rows.id'),
                    'required'      => 1,
                    'browse'        => 0,
                    'read'          => 0,
                    'edit'          => 0,
                    'add'           => 0,
                    'delete'        => 0,
                    'order'         => 1,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'pitch'
                ],
                'values'        => [
                    'type'          => 'number',
                    'display_name'  => __('panorama::seeders.data_rows.pitch'),
                    'required'      => 1,
                    'browse'        => 1,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'default'       => 0,
                        'min'           => -90,
                        'max'           => 90,
                    ],
                    'order'         => 2,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'yaw'
                ],
                'values'        => [
                    'type'          => 'number',
                    'display_name'  => __('panorama::seeders.data_rows.yaw'),
                    'required'      => 1,
                    'browse'        => 1,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'default'       => 0,
                        'min'           => -180,
                        'max'           => 180,
                    ],
                    'order'         => 3,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'type'
                ],
                'values'        => [
                    'type'          => 'select_dropdown',
                    'display_name'  => __('panorama::seeders.data_rows.type'),
                    'required'      => 1,
                    'browse'        => 1,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'default'       => Hotspot::TYPE_INFO,
                        'options'       => app(HotspotRepository::class)->getTypes(),
                    ],
                    'order'         => 4,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'text'
                ],
                'values'        => [
                    'type'          => 'text',
                    'display_name'  => __('panorama::seeders.data_rows.text'),
                    'required'      => 1,
                    'browse'        => 1,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'order'         => 5,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'URL'
                ],
                'values'        => [
                    'type'          => 'text',
                    'display_name'  => __('panorama::seeders.data_rows.url'),
                    'required'      => 0,
                    'browse'        => 1,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'order'         => 5,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'sceneId'
                ],
                'values'        => [
                    'type'          => 'select_dropdown',
                    'display_name'  => __('panorama::seeders.data_rows.scene'),
                    'required'      => 0,
                    'browse'        => 1,
                    'read'          => 1,
                    'edit'          => 1,
                    'add'           => 1,
                    'delete'        => 1,
                    'details'       => [
                        'view'          => 'panorama::formfields.select_dropdown',
                    ],
                    'order'         => 6,
                ]
            ],
            [
                'attributes'    => [
                    'field'         => 'created_at',
                ],
                'values'        => [
                    'type'          => 'timestamp',
                    'display_name'  => __('voyager::seeders.data_rows.created_at'),
                    'required'      => 0,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 0,
                    'add'           => 0,
                    'delete'        => 0,
                    'order'         => 7,
                ],
            ],
            [
                'attributes'    => [
                    'field'         => 'updated_at',
                ],
                'values'        => [
                    'type'          => 'timestamp',
                    'display_name'  => __('voyager::seeders.data_rows.updated_at'),
                    'required'      => 0,
                    'browse'        => 0,
                    'read'          => 1,
                    'edit'          => 0,
                    'add'           => 0,
                    'delete'        => 0,
                    'order'         => 8,
                ]
            ],
        ];
        $this->_buildDataRows($dataType, $dataRows);
    }

    protected function buildMenu()
    {
        //
    }

    protected function buildPermission()
    {
        $this->_buildPermission();
    }
}
