<?php

use HMT\Panorama\Models\Hotspot;
use HMT\Panorama\Models\Scene;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreatePanoramaHotspotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(app(Hotspot::class)->getTable(), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('pitch')->default(0); // degrees
            $table->float('yaw')->default(0); // degrees
            $table->enum('type', Hotspot::$types)->default(Hotspot::TYPE_INFO);
            $table->string('text')->default('');
            $table->string('URL')->nullable();
            $table->string('attributes')->nullable();
            $table->string('sceneId')->nullable();
            $table->float('targetPitch')->nullable();
            $table->float('targetYaw')->nullable();
            $table->float('targetHfov')->nullable();
            $table->string('cssClass')->nullable();
            $table->string('createTooltipFunc')->nullable();
            $table->string('createTooltipArgs')->nullable();
            $table->string('clickHandlerFunc')->nullable();
            $table->string('clickHandlerArgs')->nullable();
            $table->boolean('scale')->default(false);
            $table->unsignedBigInteger('scene_id');
            $table->foreign('scene_id')->references('id')->on(app(Scene::class)->getTable())
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(app(Hotspot::class)->getTable());
    }
}
