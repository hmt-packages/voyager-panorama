<?php

use HMT\Panorama\Models\Category;
use HMT\Panorama\Models\Scene;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreatePanoramaScenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(app(Scene::class)->getTable(), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type', Scene::$types)->default(Scene::TYPE_EQUIRECTANGULAR);
            $table->string('title')->nullable();
            $table->string('author')->nullable();
            $table->string('authorURL')->nullable();
            $table->string('strings')->nullable();
            $table->string('basePath')->nullable();
            $table->boolean('autoLoad')->default(false);
            $table->float('autoRotate')->default(0); // degrees per second
            $table->unsignedInteger('autoRotateInactivityDelay')->nullable(); // milliseconds
            $table->unsignedInteger('autoRotateStopDelay')->nullable(); // milliseconds
            $table->string('fallback')->nullable();
            $table->boolean('orientationOnByDefault')->default(false);
            $table->boolean('showZoomCtrl')->default(true);
            $table->boolean('keyboardZoom')->default(true);
            $table->boolean('mouseZoom')->default(true);
            $table->boolean('draggable')->default(true);
            $table->float('friction')->default(0.15); // (0.0, 1.0]
            $table->boolean('disableKeyboardCtrl')->default(false);
            $table->boolean('showFullscreenCtrl')->default(true);
            $table->boolean('showControls')->default(true);
            $table->float('touchPanSpeedCoeffFactor')->default(1); // Adjusts panning speed from touch inputs.
            $table->float('yaw')->default(0); // degrees
            $table->float('pitch')->default(0); // degrees
            $table->float('hfov')->default(100); // degrees
            $table->float('minYaw')->default(-180); // degrees
            $table->float('maxYaw')->default(180); // degrees
            $table->float('minPitch')->default(-90); // degrees
            $table->float('maxPitch')->default(90); // degrees
            $table->float('minHfov')->default(50); // degrees
            $table->float('maxHfov')->default(120); // degrees
            $table->boolean('multiResMinHfov')->default(false);
            $table->boolean('compass')->default(false); // boolean
            $table->float('northOffset')->nullable(); // degrees
            $table->string('preview')->nullable();
            $table->string('previewTitle')->nullable();
            $table->string('previewAuthor')->nullable();
            $table->float('horizonPitch')->nullable(); // degrees
            $table->float('horizonRoll')->nullable(); // degrees
            $table->boolean('escapeHTML')->default(false);
            $table->enum('crossOrigin', Scene::$crossOrigins)->default(Scene::CROSS_ORIGIN_ANONYMOUS);
            $table->boolean('hotSpotDebug')->default(false);
            $table->unsignedInteger('sceneFadeDuration')->nullable(); // milliseconds
            $table->text('capturedKeyNumbers')->nullable();
            $table->text('backgroundColor')->nullable();
            $table->boolean('avoidShowingBackground')->default(false);
            // equirectangular specific options
            $table->string('panorama')->nullable();
            $table->float('haov')->default(360); // degrees
            $table->float('vaov')->default(180); // degrees
            $table->float('vOffset')->default(0); // degrees
            $table->boolean('ignoreGPanoXMP')->default(false);
            // cubemap specific options
            $table->string('cubeMap')->nullable();
            // multires specific options
            $table->string('multiRes')->nullable();
            // Dynamic content specific options
            $table->boolean('dynamic')->default(false);
            $table->boolean('dynamicUpdate')->default(false);
            $table->unsignedInteger('order')->default(1);
            $table->boolean('active')->default(false);
            $table->unsignedBigInteger('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on(app(Category::class)->getTable())
                ->onUpdate('cascade')->onDelete('set null');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(app(Scene::class)->getTable());
    }
}
