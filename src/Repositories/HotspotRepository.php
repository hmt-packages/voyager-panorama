<?php

namespace HMT\Panorama\Repositories;

use HMT\Panorama\Models\Hotspot;
use HMT\Panorama\Models\Scene;
use Illuminate\Support\Arr;

class HotspotRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function getModel(): string
    {
        return Hotspot::class;
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return [
            Hotspot::TYPE_INFO => __('panorama::scenes.hotspot.types.info'),
            Hotspot::TYPE_SCENE => __('panorama::scenes.hotspot.types.scene'),
        ];
    }

    /**
     * @param  Scene       $scene
     * @param  array|null  $hotspots
     */
    public function save(Scene $scene, array $hotspots = null)
    {
        if (!$hotspots) {
            return;
        }

        foreach (Arr::except($hotspots, 'destroy') as $key => $item) {
            if (intval($key)) {
                $hotspot = Hotspot::find($key);
                $translations = $this->getTranslations($hotspot, $item);
                $hotspot->update($item);
            } else {
                $hotspot = new Hotspot();
                $hotspot->pitch = $item['pitch'];
                $hotspot->yaw = $item['yaw'];
                $hotspot->type = $item['type'];
                $hotspot->URL = $item['URL'] ?? null;
                $translations = $this->getTranslations($hotspot, $item);
                $hotspot->text = $item['text'];
                $hotspot->sceneId = $item['sceneId'] ?? null;

                $scene->hotspots()->save($hotspot);
            }

            $hotspot->saveTranslations($translations);
        }

        $this->destroyHotspot(json_decode($hotspots['destroy']));
    }

    /**
     * @param  array|int  $id
     */
    public function destroyHotspot($id)
    {
        Hotspot::destroy($id);
    }

    /**
     * @param  Hotspot  $hotspot
     * @param  array    $data
     * @return array
     */
    private function getTranslations(Hotspot $hotspot, array &$data): array
    {
        $translations = [];

        foreach ($data['i18n'] as $field => $value) {
            if (is_null($value)) {
                continue;
            }

            $trans = json_decode($value, true);
            $data[$field] = $trans[config('voyager.multilingual.default', 'en')];

            $translations[$field] = $hotspot->setAttributeTranslations($field, $trans);
        }

        return $translations;
    }
}
