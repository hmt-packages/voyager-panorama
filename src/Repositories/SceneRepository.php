<?php

namespace HMT\Panorama\Repositories;

use HMT\Panorama\Models\Hotspot;
use HMT\Panorama\Models\Scene;
use TCG\Voyager\Facades\Voyager;

class SceneRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function getModel(): string
    {
        return Scene::class;
    }

    /**
     * @param  int|null  $exceptId
     * @param  false     $isAdmin
     * @return mixed
     */
    public function get(int $exceptId = null, $isAdmin = false)
    {
        $scenes = $this->model;

        if (!$isAdmin) {
            $scenes = $scenes->active();
        }

        if ($exceptId) {
            $scenes = $scenes->where('id', '<>', $exceptId);
        }

        $scenes = $scenes->orderBy('order')->get();

        return $scenes;
    }

    /**
     * @return array
     */
    public function getReviewConfig(): array
    {
        $jsonData = [];
        $scenes = $this->get();

        if ($scenes) {
            $jsonData['default'] = [
                'firstScene'        => "scene_{$scenes->first()->id}",
                'author'            => config('app.name'),
                'sceneFadeDuration' => 1000,
            ];

            $jsonData['scenes'] = $this->getConfig();
        }

        return $jsonData;
    }

    /**
     * @param  int|null  $exceptId
     * @return array
     */
    public function getConfig(int $exceptId = null): array
    {
        $scenes = $this->get($exceptId);
        $configs = [];

        foreach ($scenes as $scene) {
            $hotspots = [];
            foreach ($scene->hotspots as $hotspot) {
                $data = [
                    'pitch' => $hotspot->pitch,
                    'yaw'   => $hotspot->yaw,
                    'type'  => $hotspot->type,
                    'text'  => $hotspot->text,
                ];

                if ($hotspot->type == Hotspot::TYPE_INFO) {
                    $data['URL'] = $hotspot->URL;
                } else {
                    $data['sceneId'] = $hotspot->sceneId;
                }

                $hotspots[] = $data;
            }

            $configs["scene_{$scene->id}"] = [
                'title'               => $scene->title,
                'hfov'                => floatval($scene->hfov),
                'pitch'               => floatval($scene->pitch),
                'yaw'                 => floatval($scene->yaw),
                'type'                => $scene->type,
                'panorama'            => Voyager::image($scene->panorama),
                'hotSpots'            => $hotspots,
                'autoLoad'            => boolval($scene->autoLoad),
                'autoRotate'          => floatval($scene->autoRotate),
                'showZoomCtrl'        => boolval($scene->showZoomCtrl),
                'showFullscreenCtrl'  => boolval($scene->showFullscreenCtrl),
                'keyboardZoom'        => boolval($scene->keyboardZoom),
                'disableKeyboardCtrl' => boolval($scene->disableKeyboardCtrl),
                'mouseZoom'           => boolval($scene->mouseZoom),
                'draggable'           => boolval($scene->draggable),
                'compass'             => boolval($scene->compass),
            ];
        }

        return $configs;
    }

    /**
     * @return string
     */
    public function getPreviewURL(): string
    {
        return route('voyager.panorama-scenes.review', ['config' => route('voyager.panorama-scenes.config')]);
    }
}
