<?php

namespace HMT\Panorama\Repositories;

use HMT\Panorama\Models\Category;

class CategoryRepository extends BaseRepository
{
    public function getModel(): string
    {
        return Category::class;
    }
}
