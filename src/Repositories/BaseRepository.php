<?php

namespace HMT\Panorama\Repositories;

abstract class BaseRepository implements RepositoryInterface
{
    protected $model;

    public function __construct()
    {
        $this->setModel();
    }

    abstract public function getModel();

    public function setModel()
    {
        $this->model = app($this->getModel());
    }

    public function find($id)
    {
        return $this->model->find($id);
    }
}
