<?php

namespace HMT\Panorama\Observers;

use HMT\Panorama\Models\Scene;
use HMT\Panorama\Repositories\HotspotRepository;
use HMT\Panorama\Repositories\SceneRepository;
use Illuminate\Http\Request;

class SceneObserver
{
    protected $repository;
    protected $hotspotRepository;
    protected $request;

    /**
     * SceneObserver constructor.
     *
     * @param  SceneRepository    $repository
     * @param  HotspotRepository  $hotspotRepository
     * @param  Request            $request
     */
    public function __construct(SceneRepository $repository, HotspotRepository $hotspotRepository, Request $request)
    {
        $this->repository = $repository;
        $this->hotspotRepository = $hotspotRepository;
        $this->request = $request;
    }

    /**
     * Handle the Scene "creating" event.
     *
     * @param  Scene  $scene
     * @return void
     */
    public function creating(Scene $scene)
    {
        $scene->order = $scene->highestOrder();
    }

    /**
     * Handle the Scene "saved" event.
     *
     * @param  Scene  $scene
     * @return void
     */
    public function saved(Scene $scene)
    {
        $this->hotspotRepository->save($scene, $this->request->get('hotspots'));
    }

    /**
     * Handle the Scene "deleted" event.
     *
     * @param  Scene  $scene
     * @return void
     */
    public function deleted(Scene $scene)
    {
        //
    }

    /**
     * Handle the Scene "restored" event.
     *
     * @param  Scene  $scene
     * @return void
     */
    public function restored(Scene $scene)
    {
        //
    }

    /**
     * Handle the Scene "force deleted" event.
     *
     * @param  Scene  $scene
     * @return void
     */
    public function forceDeleted(Scene $scene)
    {
        //
    }
}
