<?php

namespace HMT\Panorama\Http\Controllers\Admin;

use HMT\Panorama\Models\Hotspot;
use HMT\Panorama\Models\Scene;
use HMT\Panorama\Repositories\SceneRepository;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class HotspotController extends VoyagerBaseController
{
    /**
     * @var SceneRepository
     */
    private $sceneRepository;

    /**
     * HotspotController constructor.
     *
     * @param  SceneRepository  $sceneRepository
     */
    public function __construct(SceneRepository $sceneRepository)
    {
        $this->sceneRepository = $sceneRepository;
    }

    /**
     * @param  Request  $request
     * @return array
     */
    public function create(Request $request): array
    {
        $dataTypeContent = $this->sceneRepository->find($request->get('scene_id')) ?? new Scene();
        $scenes = $this->sceneRepository->get($request->get('scene_id'), true);
        $edit = false;

        $hotspot = new Hotspot();
        $hotspot->id = "new_{$request->get('key', 1)}";
        $hotspot->pitch = $request->get('pitch', 0);
        $hotspot->yaw = $request->get('yaw', 0);
        $hotspot->type = $request->get('type', Hotspot::TYPE_INFO);
        $hotspot->text = $request->get('text', '');
        $hotspot->URL = $request->get('URL', '');
        $hotspot->sceneId = $request->get('sceneId', '');

        return [
            'item' => view('panorama::scenes.hotspots.item', compact('hotspot'))->render(),
            'content' => view('panorama::scenes.hotspots.content', compact('dataTypeContent', 'scenes', 'hotspot', 'edit'))->render(),
        ];
    }
}
