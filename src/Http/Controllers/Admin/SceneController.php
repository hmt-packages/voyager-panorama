<?php

namespace HMT\Panorama\Http\Controllers\Admin;

use Exception;
use HMT\Panorama\Models\Hotspot;
use HMT\Panorama\Models\Scene;
use HMT\Panorama\Repositories\SceneRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class SceneController extends VoyagerBaseController
{
    /**
     * @var SceneRepository
     */
    private $repository;

    /**
     * SceneController constructor.
     *
     * @param  SceneRepository  $repository
     */
    public function __construct(SceneRepository $repository)
    {
        $this->repository = $repository;
        view()->share(['repository' => $this->repository]);
    }

    /**
     * @param  Request     $request
     * @param  string      $slug
     * @param  Collection  $rows
     * @param  Scene       $data
     * @return mixed
     * @throws Exception
     */
    public function insertUpdateData($request, $slug, $rows, $data)
    {
        DB::beginTransaction();

        try {
            $data = parent::insertUpdateData($request, $slug, $rows, $data);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            throw new Exception($e->getMessage());
        }

        return $data;
    }

    /**
     * @return Application|Factory|View
     */
    public function review()
    {
        return view('panorama::scenes.review');
    }

    /**
     * @return array
     */
    public function config(): array
    {
        return $this->repository->getReviewConfig();
    }
}
