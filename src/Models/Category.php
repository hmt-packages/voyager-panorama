<?php

namespace HMT\Panorama\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

/**
 * Class Category
 *
 * @property string     $name
 * @property int        $order
 * @property bool       $active
 * @property Category   $parent
 * @property Category   $children
 * @property Collection $scenes
 */
class Category extends Model
{
    use SoftDeletes, Translatable;

    protected $translatable = ['name'];

    protected $nested_level = 5;

    public function getNestedLevel(): int
    {
        return $this->nested_level;
    }

    /**
     * Get the table associated with the model.
     *
     * @return string
     */
    public function getTable(): string
    {
        return config('panorama.table_prefix', 'panorama_') . 'categories';
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Scope a query to only include active categories.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('active', true);
    }

    /**
     * Get the scenes for the category.
     *
     * @return HasMany
     */
    public function scenes(): HasMany
    {
        return $this->hasMany(Scene::class, 'category_id');
    }

    /**
     * Get the parent that owns the category.
     *
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(self::class);
    }

    /**
     * Get the children for the category.
     *
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(self::class, 'parent_id');
    }
}
