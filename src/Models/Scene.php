<?php

namespace HMT\Panorama\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

/**
 * Class Scene
 *
 * @property string   $type                      This specifies the panorama type. Can be equirectangular, cubemap, or multires. Defaults to equirectangular.
 * @property string   $title                     If set, the value is displayed as the panorama’s title. If no title is desired, don’t set this parameter.
 * @property string   $author                    If set, the value is displayed as the panorama’s author. If no author is desired, don’t set this parameter.
 * @property string   $authorURL                 If set, the displayed author text is hyperlinked to this URL. If no author URL is desired, don’t set this parameter. The author parameter must also be set for this parameter to have an effect.
 * @property string   $strings                   Allows user-facing strings to be changed / translated. See defaultConfig.strings definition in pannellum.js for more details.
 * @property string   $basePath                  This specifies a base path to load the images from.
 * @property float    $autoLoad                  When set to true, the panorama will automatically load. When false, the user needs to click on the load button to load the panorama. Defaults to false.
 * @property float    $autoRotate                Setting this parameter causes the panorama to automatically rotate when loaded. The value specifies the rotation speed in degrees per second. Positive is counter-clockwise, and negative is clockwise.
 * @property integer  $autoRotateInactivityDelay Sets the delay, in milliseconds, to start automatically rotating the panorama after user activity ceases. This parameter only has an effect if the autoRotate parameter is set. Before starting rotation, the viewer is panned to the initial pitch.
 * @property integer  $autoRotateStopDelay       Sets the delay, in milliseconds, to stop automatically rotating the panorama after it is loaded. This parameter only has an effect if the autoRotate parameter is set.
 * @property string   $fallback                  If set, the value is used as a URL for a fallback viewer in case Pannellum is not supported by the user’s device. The user will be given the option to click a link and visit this URL if Pannellum fails to work.
 * @property boolean  $orientationOnByDefault    If set to true, device orientation control will be used when the panorama is loaded, if the device supports it. If false, device orientation control needs to be activated by pressing a button. Defaults to false.
 * @property boolean  $showZoomCtrl              If set to false, the zoom controls will not be displayed. Defaults to true.
 * @property boolean  $keyboardZoom              If set to false, zooming with keyboard will be disabled. Defaults to true.
 * @property boolean  $mouseZoom                 If set to false, zooming with mouse wheel will be disabled. Defaults to true. Can also be set to fullscreenonly, in which case it is only enabled when the viewer is fullscreen.
 * @property boolean  $draggable                 If set to false, mouse and touch dragging is disabled. Defaults to true.
 * @property float    $friction                  Controls the “friction” that slows down the viewer motion after it is dragged and released. Higher values mean the motion stops faster. Should be set (0.0, 1.0]; defaults to  0.15.
 * @property boolean  $disableKeyboardCtrl       If set to true, keyboard controls are disabled. Defaults to false.
 * @property boolean  $showFullscreenCtrl        If set to false, the fullscreen control will not be displayed. Defaults to true. The fullscreen button will only be displayed if the browser supports the fullscreen API.
 * @property boolean  $showControls              If set to false, no controls are displayed. Defaults to true.
 * @property float    $touchPanSpeedCoeffFactor  Adjusts panning speed from touch inputs. Defaults to 1.
 * @property float    $yaw                       Sets the panorama’s starting yaw position in degrees. Defaults to 0.
 * @property float    $pitch                     Sets the panorama’s starting pitch position in degrees. Defaults to 0.
 * @property float    $hfov                      Sets the panorama’s starting horizontal field of view in degrees. Defaults to 100.
 * @property float    $minYaw                    Sets the minimum yaw the viewer edge can be at, in degrees. Defaults to -180 / 180, i.e. no limit.
 * @property float    $maxYaw                    Sets the maximum yaw the viewer edge can be at, in degrees. Defaults to -180 / 180, i.e. no limit.
 * @property float    $minPitch                  Sets the minimum pitch the viewer edge can be at, in degrees. Defaults to undefined, so the viewer center can reach -90 / 90.
 * @property float    $maxPitch                  Sets the maximum pitch the viewer edge can be at, in degrees. Defaults to undefined, so the viewer center can reach -90 / 90.
 * @property float    $minHfov                   Sets the minimum horizontal field of view, in degrees, that the viewer can be set to. Defaults to 50 / 120. Unless the multiResMinHfov parameter is set to true, the minHfov parameter is ignored for multires panoramas.
 * @property float    $maxHfov                   Sets the maximum horizontal field of view, in degrees, that the viewer can be set to. Defaults to 50 / 120. Unless the multiResMinHfov parameter is set to true, the minHfov parameter is ignored for multires panoramas.
 * @property boolean  $multiResMinHfov           When set to false, the minHfov parameter is ignored for multires panoramas; an automatically calculated minimum horizontal field of view is used instead. Defaults to false.
 * @property boolean  $compass                   If true, a compass is displayed. Normally defaults to false; defaults to true if heading information is present in Photo Sphere XMP metadata.
 * @property float    $northOffset               Set the offset, in degrees, of the center of the panorama from North. As this affects the compass, it only has an effect if compass is set to true.
 * @property string   $preview                   Specifies a URL for a preview image to display before the panorama is loaded.
 * @property string   $previewTitle              Specifies the title to be displayed while the load button is displayed.
 * @property string   $previewAuthor             Specifies the author to be displayed while the load button is displayed.
 * @property string   $horizonPitch              Specifies pitch of image horizon, in degrees (for correcting non-leveled panoramas).
 * @property string   $horizonRoll               Specifies roll of image horizon, in degrees (for correcting non-leveled panoramas).
 * @property boolean  $escapeHTML                When true, HTML is escaped from configuration strings to help mitigate possible DOM XSS attacks. This is always true when using the standalone viewer since the configuration is provided via the URL; it defaults to false but can be set to true when using the API.
 * @property string   $crossOrigin               This specifies the type of CORS request used and can be set to either anonymous or use-credentials. Defaults to anonymous.
 * @property Hotspot  $hotSpots                  This specifies a dictionary of hot spots that can be links to other scenes, information, or external links. Each array element has the following properties.
 * @property boolean  $hotSpotDebug              When true, the mouse pointer’s pitch and yaw are logged to the console when the mouse button is clicked. Defaults to false.
 * @property integer  $sceneFadeDuration         Specifies the fade duration, in milliseconds, when transitioning between scenes. Not defined by default. Only applicable for tours. Only works with WebGL renderer.
 * @property array    $capturedKeyNumbers        Specifies the key numbers that are captured in key events. Defaults to the standard keys that are used by the viewer.
 * @property array    $backgroundColor           Specifies an array containing RGB values [0, 1] that sets the background color for areas where no image data is available. Defaults to [0, 0, 0] (black). For partial equirectangular panoramas this applies to areas past the edges of the defined rectangle. For multires and cubemap (including fallback) panoramas this applies to areas corresponding to missing tiles or faces.
 * @property boolean  $avoidShowingBackground    If set to true, prevent displaying out-of-range areas of a partial panorama by constraining the yaw and the field-of-view. Even at the corners and edges of the canvas only areas actually belonging to the image (i.e., within [minYaw, maxYaw] and [minPitch, maxPitch]) are shown, thus setting the backgroundColor option is not needed if this option is set. Defaults to false.
 * @todo equirectangular specific options
 * @property string   $panorama                  Sets the URL to the equirectangular panorama image. This is relative to basePath if it is set, else it is relative to the location of pannellum.htm. An absolute URL can also be used.
 * @property float    $haov                      Sets the panorama’s horizontal angle of view, in degrees. Defaults to 360. This is used if the equirectangular image does not cover a full 360 degrees in the horizontal.
 * @property float    $vaov                      Sets the panorama’s vertical angle of view, in degrees. Defaults to 180. This is used if the equirectangular image does not cover a full 180 degrees in the vertical.
 * @property float    $vOffset                   Sets the vertical offset of the center of the equirectangular image from the horizon, in degrees. Defaults to 0. This is used if vaov is less than 180 and the equirectangular image is not cropped symmetrically.
 * @property boolean  $ignoreGPanoXMP            If set to true, any embedded Photo Sphere XMP data will be ignored; else, said data will override any existing settings. Defaults to false.
 * @todo cubemap specific options
 * @property string   $cubeMap                   This is an array of URLs for the six cube faces in the order front, right, back, left, up, down. These are relative to basePath if it is set, else they are relative to the location of pannellum.htm. Absolute URLs can also be used. Partial cubemap images may be specified by giving null instead of a URL.
 * @todo multires specific options
 * @property string   $multiRes                  This contains information about the multiresolution panorama in sub-keys.
 * @todo Dynamic content specific options
 * @property boolean  $dynamic                   The panorama source is considered dynamic when this is set to true. Defaults to false. This should be set to true for video.
 * @property boolean  $dynamicUpdate             For dynamic content, viewer will start automatically updating when set to true. Defaults to false. If the updates are controlled via the setUpdate method, as with the Video.js plugin, this should be set to false.
 *
 * @property int      $order
 * @property Category $category
 * @property Hotspot  $hotspots
 */
class Scene extends Model
{
    use SoftDeletes, Translatable;

    protected $translatable = ['title'];
    protected $mainFields = ['title', 'active', 'yaw', 'pitch', 'hfov'];
    protected $subFields = [
        'media' => ['panorama'],
        'options' => ['autoRotate', 'autoLoad', 'showZoomCtrl', 'showFullscreenCtrl', 'keyboardZoom', 'disableKeyboardCtrl', 'mouseZoom', 'draggable', 'compass'],
        'relationships' => ['panorama_scene_belongsto_panorama_category_relationship'],
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'scenes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'active', 'yaw', 'pitch', 'hfov',
        'panorama',
        'category_id',
        'autoRotate', 'autoLoad', 'showZoomCtrl', 'showFullscreenCtrl', 'keyboardZoom', 'disableKeyboardCtrl', 'mouseZoom', 'draggable', 'compass',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Types.
     */
    public const TYPE_EQUIRECTANGULAR = 'equirectangular';
    public const TYPE_CUBEMAP = 'cubemap';
    public const TYPE_MULTIRES = 'multires';

    /**
     * Cross Origins.
     */
    public const CROSS_ORIGIN_ANONYMOUS = 'anonymous';
    public const CROSS_ORIGIN_USE_CREDENTIALS = 'use-credentials';

    /**
     * List of types.
     *
     * @var array
     */
    public static $types = [self::TYPE_EQUIRECTANGULAR, self::TYPE_CUBEMAP, self::TYPE_MULTIRES];

    /**
     * List of cross origins.
     *
     * @var array
     */
    public static $crossOrigins = [self::CROSS_ORIGIN_ANONYMOUS, self::CROSS_ORIGIN_USE_CREDENTIALS];

    /**
     * Get the table associated with the model.
     *
     * @return string
     */
    public function getTable(): string
    {
        return config('panorama.table_prefix', 'panorama_') . 'scenes';
    }

    public function getMainFields(): array
    {
        return $this->mainFields;
    }

    public function getSubFields(): array
    {
        return $this->subFields;
    }

    /**
     * Get the category that owns the scene.
     *
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('active', true);
    }

    /**
     * Get the hotspots for the scene.
     *
     * @return HasMany
     */
    public function hotspots(): HasMany
    {
        return $this->hasMany(Hotspot::class);
    }

    /**
     * Return the Highest Order Scene.
     *
     * @return int
     */
    public function highestOrder(): int
    {
        $order = 1;

        $item = $this->orderBy('order', 'DESC')->first();

        if ($item) {
            $order += intval($item->order);
        }

        return $order;
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
//            $model->order = $model->highestOrder();
        });

        static::saving(function ($model) {
//            $hotSpots = array_values(request()->get('hotSpots', []));
//
//            foreach ($hotSpots as $key => $value) {
//                $hotSpots[$key]['id'] = "hotspot_{$key}";
//            }
//
//            $model->hotSpots = json_encode($hotSpots);
        });

        static::saved(function ($model) {
//            app(SceneRepository::class)->saveHotspots($model, request()->get('hotspots'));
        });
    }
}
