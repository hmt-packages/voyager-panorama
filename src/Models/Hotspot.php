<?php

namespace HMT\Panorama\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

/**
 * Class Hotspot
 *
 * @property integer $pitch             Specifies the pitch portion of the hot spot’s location, in degrees.
 * @property integer $yaw               Specifies the yaw portion of the hot spot’s location, in degrees.
 * @property string  $type              Specifies the type of the hot spot. Can be scene for scene links or info for information hot spots. A tour configuration file is required for scene hot spots.
 * @property string  $text              This specifies the text that is displayed when the user hovers over the hot spot.
 * @property string  $URL               If specified for an info hot spot, the hot spot links to the specified URL. Not applicable for scene hot spots.
 * @property string  $attributes        Specifies URL’s link attributes. If not set, the target attribute is set to _blank, to open link in new tab to avoid opening in viewer frame / page.
 * @property string  $sceneId           Specifies the ID of the scene to link to for scene hot spots. Not applicable for info hot spots.
 * @property integer $targetPitch       Specifies the pitch of the target scene, in degrees. Can also be set to same, which uses the current pitch of the current scene as the initial pitch of the target scene.
 * @property integer $targetYaw         Specifies the yaw of the target scene, in degrees. Can also be set to same or sameAzimuth. These settings use the current yaw of the current scene as the initial yaw of the target scene; same uses the current yaw directly, while sameAzimuth takes into account the northOffset values of both scenes to maintain the same direction with regard to north.
 * @property integer $targetHfov        Specifies the HFOV of the target scene, in degrees. Can also be set to same, which uses the current HFOV of the current scene as the initial HFOV of the target scene.
 * @property string  $id                Specifies hot spot ID, for use with API’s removeHotSpot function.
 * @property string  $cssClass          If specified, string is used as the CSS class for the hot spot instead of the default CSS classes.
 * @property string  $createTooltipFunc If createTooltipFunc is specified, this function is used to create the hot spot tooltip DOM instead of the default function.
 * @property string  $createTooltipArgs The contents of createTooltipArgs are passed to the createTooltipFunc function as arguments.
 * @property string  $clickHandlerFunc  If clickHandlerFunc is specified, this function is added as an event handler for the hot spot’s click event.
 * @property string  $clickHandlerArgs  The event object and the contents of clickHandlerArgs are passed to the clickHandlerFunc function as arguments.
 * @property boolean $scale             When true, the hot spot is scaled to match changes in the field of view, relative to the initial field of view. Note that this does not account for changes in local image scale that occur due to distortions within the viewport. Defaults to false.
 * @property Scene   $scene
 */
class Hotspot extends Model
{
    use SoftDeletes, Translatable;

    protected $translatable = ['text'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hotspots';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['pitch', 'yaw', 'type', 'text', 'URL', 'sceneId'];

    /**
     * Get the table associated with the model.
     *
     * @return string
     */
    public function getTable(): string
    {
        return config('panorama.table_prefix', 'panorama_') . 'hotspots';
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Types.
     */
    public const TYPE_SCENE = 'scene';
    public const TYPE_INFO = 'info';

    /**
     * List of types.
     *
     * @var array
     */
    public static $types = [self::TYPE_SCENE, self::TYPE_INFO];

    /**
     * Get the scene that owns the hotspot.
     *
     * @return BelongsTo
     */
    public function scene(): BelongsTo
    {
        return $this->belongsTo(Scene::class);
    }
}
