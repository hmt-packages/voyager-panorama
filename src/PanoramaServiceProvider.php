<?php

namespace HMT\Panorama;

use HMT\Panorama\Models\Category;
use HMT\Panorama\Models\Scene;
use HMT\Panorama\Observers\SceneObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class PanoramaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'panorama');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'panorama');

        $this->registerPublishableResources();

        Scene::observe(SceneObserver::class);
    }

    private function registerPublishableResources()
    {
        $categoryTableSlug = Str::slug(app(Category::class)->getTable());
        $sceneTableSlug = Str::slug(app(Scene::class)->getTable());

        $publishable = [
            'config' => [
                __DIR__.'/../config/panorama.php' => config_path('panorama.php'),
            ],
            'lang' => [
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/panorama'),
            ],
            'migrations' => [
                __DIR__.'/../database/migrations/' => database_path('migrations'),
            ],
            'seeders' => [
                __DIR__.'/../database/seeders/' => database_path('seeders'),
            ],
            'views' => [
                __DIR__.'/../resources/views/categories' => resource_path("views/vendor/voyager/{$categoryTableSlug}"),
                __DIR__.'/../resources/views/formfields' => resource_path('views/vendor/panorama/formfields'),
                __DIR__.'/../resources/views/partials' => resource_path("views/vendor/panorama/partials"),
                __DIR__.'/../resources/views/scenes' => resource_path("views/vendor/voyager/{$sceneTableSlug}"),
            ],
            'assets' => [
                __DIR__.'/../resources/css/' => public_path('assets/admin/panorama/css'),
                __DIR__.'/../resources/js/' => public_path('assets/admin/panorama/js'),
            ],
        ];

        foreach ($publishable as $group => $paths) {
            $this->publishes($paths, "panorama-{$group}");
        }
    }
}
