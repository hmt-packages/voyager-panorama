# Voyager Panorama

Voyager Panorama Package

* php artisan voyager:install --with-dummy
* php artisan vendor:publish --tag=panorama-assets
* php artisan vendor:publish --tag=panorama-config
* php artisan vendor:publish --tag=panorama-views
* php artisan db:seed --class="HMT\Panorama\Database\Seeders\CategorySeeder"
* php artisan db:seed --class="HMT\Panorama\Database\Seeders\SceneSeeder"
* php artisan db:seed --class="HMT\Panorama\Database\Seeders\HotspotSeeder"
